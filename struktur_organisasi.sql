create table jabatan
(
    id   int auto_increment,
    nama int null,
    constraint jabatan_pk
        primary key (id)
);

insert into jabatan(nama)
values ('Direktur'),
       ('Manajer'),
       ('Staff');

alter table karyawan
    add jabatan int null;

alter table karyawan
    add constraint jabatan_fk
        foreign key (jabatan) references jabatan (id);

UPDATE karyawan SET jabatan = 1 WHERE nama = 'Rizki Saputra';
UPDATE karyawan SET jabatan = 2 WHERE nama IN ('Farhan Reza', 'Riyando Adi');
UPDATE karyawan SET jabatan = 3 WHERE id > 3;

SELECT k.nama as 'Bawahan Farhan Reza', d.nama as 'Nama Departemen' from karyawan k join departemen d on k.Departemen = d.id WHERE jabatan = 3 && Departemen IN (2, 3) ;
SELECT k.nama as 'Bawahan Riyando Adi', d.nama as 'Nama Departemen' from karyawan k join departemen d on k.Departemen = d.id WHERE jabatan = 3 && Departemen = 4 ;